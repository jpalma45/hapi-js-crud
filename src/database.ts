import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost:27017/dbHapi',{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
.then(() => console.log('starting database'))
.catch(err => console.log(err))
 